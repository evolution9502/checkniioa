package com.megabank.tw.checkNiioa;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.CollectingAlertHandler;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class GetNiioaStatus {
	
	public static String getResult(String uino, String bdate) {
		//初始化錯誤視窗handler
		CollectingAlertHandler alertHandler = new CollectingAlertHandler();
		//初始化模擬瀏覽器
		WebClient webclient = new WebClient(BrowserVersion.BEST_SUPPORTED);
		//開啟模擬瀏覽器JavaScript效果
		webclient.getOptions().setJavaScriptEnabled(true);
		//瀏覽器獲取錯誤視窗handler
		webclient.setAlertHandler(alertHandler);
		//錯誤訊息
		List<String> alertmsgs = new ArrayList<String>();  
		//宣告頁面
		HtmlPage htmlpage, resultPage=null;
		File img_file = null;
		
		try {
			htmlpage = webclient.getPage("https://niioa.immigration.gov.tw/uino/");
			HtmlForm formbody = htmlpage.getFormByName("uinoForm");
			HtmlTextInput input_id = formbody.getInputByName("rq.uino");
			HtmlTextInput input_birthDate = formbody.getInputByName("rq.birthDate");
			HtmlTextInput verifyCode = formbody.getInputByName("verifyCode");
			HtmlButton submit = formbody.getButtonByName("confirm");
			HtmlImage verify_img = (HtmlImage) htmlpage.getElementById("verify");
			
			UUID randomUUID = UUID.randomUUID();
			String filePath = System.getProperty("user.dir")+"/test_img/"+randomUUID.toString()+".jpg" ;
		    File directory = new File(System.getProperty("user.dir")+"/test_img");
		    if (! directory.exists()){
		        directory.mkdir();
		    }

		    img_file = new File( filePath ) ;
			verify_img.saveAs(img_file);
			String solvedCaptcha = GetCaptcha.getCaptcha(filePath);
			
			input_id.setValueAttribute(uino);
			input_birthDate.setValueAttribute(bdate);
			verifyCode.setValueAttribute(solvedCaptcha);
			//verifyCode.setValueAttribute("1234");

			//HtmlPage resultPage = submit.click();    
			submit.click();    
			alertmsgs = alertHandler.getCollectedAlerts();    
			webclient.waitForBackgroundJavaScript(3000);    
			resultPage = (HtmlPage) htmlpage.getEnclosingWindow().getTopWindow().getEnclosedPage();
			
			System.out.println("========================================");
			System.out.println("html result as text : " + resultPage.asNormalizedText());
			System.out.println("========================================");
			System.out.println("alert window msg: " + alertmsgs);
			
		} catch (FailingHttpStatusCodeException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} finally {
			webclient.close();
		}
		
		if(resultPage.asNormalizedText().contains("申請人已換領新統號")) {
			//結果為 查詢證號已換新
			img_file.delete();
			return "NEW_UINO_GRANTED";
		} else {
			if (resultPage.asNormalizedText().contains("申請人未換領新統號")) {
				//結果為 查詢證號未換新
				img_file.delete();
				return "NO_NEW_UINO";
			} else if (resultPage.asNormalizedText().contains("Please enter a valid")){
				//結果為 輸入證號或生日資料格式錯誤
				img_file.delete();
				return "DATA_FORMAT_INVALID";
			} else if (alertmsgs.get(0).contains("申請人資料輸入錯誤或均無新舊統號")) {
				//結果為 輸入資料未查詢到任何結果
				img_file.delete();
				return "INPUT_DATA_NOT_FOUND";
			} else if (alertmsgs.contains("Wrong Verification Code")) {
				//結果為 驗證碼驗證錯誤，如為此結果建議重試
				return "WRONG_CAPTCHA";
			} else {
				// 結果為 未知錯誤
				return "UNKNOWN_ERROR";
			}
		}
		
	}

}
