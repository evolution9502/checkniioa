package com.megabank.tw.checkNiioa;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class GetCaptcha {
	public static void main(String[] args) {
		String recognizeResult = null; // 定義變數，接收識別結果
		String url = "https://niioa.immigration.gov.tw/uino/imageCodeAction.action" ;
		String filePath = "/Users/mega011601/Desktop/test_img/verifyCode.jpg" ;
		File file = new File( filePath ) ;
		Path path = Paths.get(filePath);

		try {
			//把服务器上图片下载到本地F盘的abc.jpg图片
			FileUtils.copyURLToFile( new URL( url ) , file );
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("檔案大小 : " + Files.size(path));
			if (Files.size(path) > 1000) {
				toBinary(file);
			}
			System.out.println("二階化後檔案大小 : " + Files.size(path));
			ITesseract tesseract = new Tesseract();
			tesseract.setDatapath(System.getProperty("user.dir") + "/tessdata");// 設定訓練庫路徑
			//String trainPath = System.getProperty("user.dir") + "/tessdata";
			BufferedImage textImage = ImageIO.read(file); // 將圖片載入到記憶體
			tesseract.setLanguage("eng");
			//使用舊版本模式，並設置只識別為數字，不是用於識別驗證碼可不加下面兩行
			tesseract.setOcrEngineMode(1);
			tesseract.setTessVariable("tessedit_char_whitelist","0123456789");

			recognizeResult = tesseract.doOCR(textImage);// 呼叫識別方法，得到識別結果
			System.out.println("ocr result is "+ recognizeResult);
		} catch (IOException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} catch (TesseractException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		}
	}
	
	public static String getCaptcha(String filePath) {
		String recognizeResult = null; // 定義變數，接收識別結果
		File file = new File( filePath ) ;
		Path path = Paths.get(filePath);
		try {
			System.out.println("驗證碼圖形檔案大小 : " + Files.size(path));
			if (Files.size(path) > 900) {
				toBinary(file);
			}
			System.out.println("驗證碼圖形二階化後檔案大小 : " + Files.size(path));
			ITesseract tesseract = new Tesseract();
			tesseract.setDatapath(System.getProperty("user.dir") + "/tessdata");// 設定訓練庫路徑
			//String trainPath = System.getProperty("user.dir") + "/tessdata";
			BufferedImage textImage = ImageIO.read(file); // 將圖片載入到記憶體
			tesseract.setLanguage("eng");
			//使用舊版本模式，並設置只識別為數字，不是用於識別驗證碼可不加下面兩行
			tesseract.setOcrEngineMode(1);
			tesseract.setTessVariable("tessedit_char_whitelist","0123456789");

			recognizeResult = tesseract.doOCR(textImage);// 呼叫識別方法，得到識別結果
			System.out.println("ocr result is "+ recognizeResult);
		} catch (IOException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} catch (TesseractException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		}		
		
		return recognizeResult.trim();
	}
	
	
	public static void toBinary(File imageFile) throws IOException {
	    BufferedImage image = ImageIO.read(imageFile);
	    int w = image.getWidth();
	    int h = image.getHeight();
	    float[] rgb = new float[3];
	    double[][] pos = new double[w][h];
	    BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_BINARY);
	    for (int x = 0; x < w; x++) {
	      for (int y = 0; y < h; y++) {
	        int pixel = image.getRGB(x, y);
	        rgb[0] = (pixel & 0xff0000) >> 16;
	        rgb[1] = (pixel & 0xff00) >> 8;
	        rgb[2] = (pixel & 0xff);
	        float avg = (rgb[0] + rgb[1] + rgb[2]) / 3;
	        pos[x][y] = avg;
	      }
	    }
	    double SW = 170;
	    for (int x = 0; x < w; x++) {
	      for (int y = 0; y < h; y++) {
	        if (pos[x][y] <= SW) {
	          int max = new Color(0, 0, 0).getRGB();
	          bi.setRGB(x, y, max);
	        } else {
	          int min = new Color(255, 255, 255).getRGB();
	          bi.setRGB(x, y, min);
	        }
	      }
	    }
	    ImageIO.write(bi, "png", imageFile);
	  }
}
