package com.megabank.tw.checkNiioa.test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.CollectingAlertHandler;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import com.megabank.tw.checkNiioa.GetCaptcha;

public class HtmlUnitTest02 {

	public static void main(String[] args) {
		// TODO 自動產生的方法 Stub
		CollectingAlertHandler alertHandler = new CollectingAlertHandler();
		WebClient webclient = new WebClient(BrowserVersion.BEST_SUPPORTED);
		//webclient.getOptions().setCssEnabled(false);
		webclient.getOptions().setJavaScriptEnabled(true);
		webclient.setAlertHandler(alertHandler);
		List<String> alertmsgs = new ArrayList<String>();   
		
		HtmlPage htmlpage;
		try {
			htmlpage = webclient.getPage("https://niioa.immigration.gov.tw/uino/");
			System.out.println("原始網頁html as text : " + htmlpage.asNormalizedText());
			HtmlForm formbody = htmlpage.getFormByName("uinoForm");
			HtmlTextInput input_id = formbody.getInputByName("rq.uino");
			HtmlTextInput input_birthDate = formbody.getInputByName("rq.birthDate");
			HtmlTextInput verifyCode = formbody.getInputByName("verifyCode");
			HtmlButton submit = formbody.getButtonByName("confirm");
			HtmlImage verify_img = (HtmlImage) htmlpage.getElementById("verify");
			//HtmlAnchor submit = htmlpage.getHtmlElementById("divConfirm");
			
			UUID randomUUID = UUID.randomUUID();
			String filePath = "/Users/mega011601/Desktop/test_img/"+randomUUID.toString()+".jpg" ;
			File file = new File( filePath ) ;
			verify_img.saveAs(file);
			String solvedCaptcha = GetCaptcha.getCaptcha(filePath);
			
			input_id.setValueAttribute("HC04065294");
			input_birthDate.setValueAttribute("19881207");
			verifyCode.setValueAttribute(solvedCaptcha);

			//HtmlPage resultPage = submit.click();    
			submit.click();    
			alertmsgs = alertHandler.getCollectedAlerts();    
			webclient.waitForBackgroundJavaScript(3000);    
			HtmlPage resultPage = (HtmlPage) htmlpage.getEnclosingWindow().getTopWindow().getEnclosedPage();
			
			System.out.println("========================================");
			System.out.println("網頁html as text : " + resultPage.asNormalizedText());
			System.out.println("========================================");
			System.out.println("alert : " + alertmsgs);
			
		} catch (FailingHttpStatusCodeException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		} finally {
			webclient.close();
		}


	}

}
